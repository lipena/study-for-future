package com.desin.modle.behavioral.strategy;

import java.util.HashMap;
import java.util.Map;

public class PromotionStrategyFactory {
    private final static Map<String,PromotionStrategy> STRING_PROMOTION_STRATEGY_MAP = new HashMap<String, PromotionStrategy>();

    static {
        STRING_PROMOTION_STRATEGY_MAP.put(PromotionStrategyKey.chuxiao,new ChuXiaoPromotionStrategy());
        STRING_PROMOTION_STRATEGY_MAP.put(PromotionStrategyKey.manjian,new ManJianPromotionStrategy());
    }

    private PromotionStrategyFactory(){}

    public static PromotionStrategy getPromotionStrategy(String key){
        PromotionStrategy promotionStrategy = STRING_PROMOTION_STRATEGY_MAP.get(key);
        return promotionStrategy == null ? new MoRenPromotionStrategy():promotionStrategy;
    }

    private interface PromotionStrategyKey{
        String manjian = "manjian";
        String chuxiao = "chuxiao";
    }
}
