package com.desin.modle.behavioral.strategy;

public interface PromotionStrategy {
    void doPromotion();
}
