package com.desin.modle.behavioral.strategy;

/**
 * 策略模式
 *
 * 可以消除if else，避免对象中出现大方法
 *
 * 策略与策略之间可以有继承关系
 *
 * 适用:系统有很多类，而他们的区别仅仅在于他们的行为不同
 * 一个系统需要动态的在几种算法中选择一种
 *
 * 优点：开闭原则
 * 避免使用多重条件转移语句
 * 提高算法的保密性和安全性
 *
 * Comparator,TreeMap,Resource,InstantiationStrategy(bean容器初始化)
 */
public class Test {
//    public static void main(String[] args) {
//        PromotionActivity chuXiaoPromotionActivity = new PromotionActivity(new ChuXiaoPromotionStrategy());
//        PromotionActivity manJianpromotionActivity = new PromotionActivity(new ManJianPromotionStrategy());
//
//        chuXiaoPromotionActivity.executePromotionStrategy();
//        manJianpromotionActivity.executePromotionStrategy();
//    }
public static void main(String[] args) {
    //策略模式与工厂模式相结合,消除if else
    String key = "manjian";
    PromotionActivity promotionActivity = new PromotionActivity(PromotionStrategyFactory.getPromotionStrategy(key));
    promotionActivity.executePromotionStrategy();
}

}
