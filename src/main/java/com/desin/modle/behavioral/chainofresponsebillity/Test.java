package com.desin.modle.behavioral.chainofresponsebillity;

/**
 * 责任链模式
 *
 * 抽象责任者Approver,包含下一个抽象责任者对象,包含传递下一个责任者方法,包含执行下一个责任链的方法
 *
 * 具体责任者继承抽象责任者,
 *
 * Approver其实就是我们常用的Handle,更多的是filter
 *
 * 适用:一个请求的处理需要多个对象中的一个或者几个来协调处理
 *
 * 优点:请求的发送者和接受者解耦
 * 可以动态组合
 *
 * java.servlet.Filter,spring security
 */
public class Test {
    public static void main(String[] args) {
        Course course = new Course();
        course.setName("Java课程");
        course.setArticle("Java手记");
//        course.setVedio("Java视频");

        Approver articleApprover = new ArticleApprover();
        Approver vedioApprover = new VedioApprover();

        //传递下一个责任链
        articleApprover.setNextApprover(vedioApprover);

        articleApprover.deploy(course);
    }
}
