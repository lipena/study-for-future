package com.desin.modle.behavioral.chainofresponsebillity;

import org.springframework.util.StringUtils;

/**
 * 具体责任链
 */
public class ArticleApprover extends Approver {
    @Override
    public void deploy(Course course) {
        if(!StringUtils.isEmpty(course.getArticle())){
            System.out.println(course.getName() + "含有" + course.getArticle() + "Article");
            //进行判空操作，防止下一个责任器为空造成空指针
            if(approver != null){
                approver.deploy(course);
            }
        }else {
            System.out.println(course.getName() + "不含有Article，不批准");
        }

    }
}
