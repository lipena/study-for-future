package com.desin.modle.behavioral.chainofresponsebillity;

/**
 * 抽象批准者
 */
public abstract class Approver {
    /**
     * 下一个具体责任链
     *
     * protected是为了只有子类能够用
     */
    protected Approver approver;

    /**
     * 添加责任链
     * @param approver
     */
    public void setNextApprover(Approver approver) {
        this.approver = approver;
    }

    /**
     * 执行下一个责任链的方法
     * @param course
     */
    public abstract void deploy(Course course);
}
