package com.desin.modle.behavioral.observer;

import java.util.Observable;

//被观察者
public class Course extends Observable {
    private String courseName;

    public Course(String courseName) {
        this.courseName = courseName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void produceQuestion(Course course,Question question){
        System.out.println(question.getUserName() + "对" + course.getCourseName() + "提交了" + question.getQuestionContent() + "的问题");
        setChanged();
        notifyObservers(question);
    }
}
