package com.desin.modle.behavioral.observer;

//观察者模式

/**
 *
 * 适用场景:关联行为场景，建立一套触发机制
 *
 * 优点:观察者和被观察者之间建立一个抽象的耦合
 * 观察者模式支持广播通信
 *
 * java.awt.Event
 * Guava
 */
public class Test {
    public static void main(String[] args) {
        Course course = new Course("Java课程");
        Teacher teacher = new Teacher("lipeng");
        Teacher teacher1 = new Teacher("lipeng1");

        course.addObserver(teacher);
        course.addObserver(teacher1);

        Question question = new Question();
        question.setUserName("xiaowang");
        question.setQuestionContent("难不难");

        course.produceQuestion(course,question);
    }
}
