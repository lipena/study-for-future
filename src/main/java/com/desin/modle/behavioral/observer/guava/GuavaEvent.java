package com.desin.modle.behavioral.observer.guava;

import com.google.common.eventbus.Subscribe;

public class GuavaEvent {
    @Subscribe
    public void collect(String arg){
        System.out.println(arg);
    }
}
