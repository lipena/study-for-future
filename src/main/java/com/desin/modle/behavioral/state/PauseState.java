package com.desin.modle.behavioral.state;

public class PauseState extends CourseVedioState {
    @Override
    public void play() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.PLAY_STATE);
    }

    @Override
    public void speed() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.SPEED_STATE);
    }

    @Override
    public void pause() {
        System.out.println("视频已经暂停");
    }

    @Override
    public void stop() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.STOP_STATE);
    }
}
