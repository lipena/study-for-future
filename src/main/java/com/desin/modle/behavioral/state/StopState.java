package com.desin.modle.behavioral.state;

public class StopState extends CourseVedioState {
    @Override
    public void play() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.PLAY_STATE);
    }

    @Override
    public void speed() {
        System.out.println("Error 停止不能快进");
    }

    @Override
    public void pause() {
        System.out.println("Error 停止不能暂停");
    }

    @Override
    public void stop() {
        System.out.println("视频已经停止");
    }
}
