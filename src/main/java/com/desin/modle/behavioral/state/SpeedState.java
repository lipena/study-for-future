package com.desin.modle.behavioral.state;

public class SpeedState extends CourseVedioState {
    @Override
    public void play() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.PLAY_STATE);
    }

    @Override
    public void speed() {
        System.out.println("视频正常快进");
    }

    @Override
    public void pause() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.PAUSE_STATE);
    }

    @Override
    public void stop() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.STOP_STATE);
    }
}
