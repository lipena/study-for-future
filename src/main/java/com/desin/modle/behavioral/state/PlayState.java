package com.desin.modle.behavioral.state;

public class PlayState extends CourseVedioState {
    @Override
    public void play() {
        System.out.println("视频已经在播放");
    }

    @Override
    public void speed() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.SPEED_STATE);
    }

    @Override
    public void pause() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.PAUSE_STATE);
    }

    @Override
    public void stop() {
        this.courseVedioContext.setCourseVedioState(CourseVedioContext.STOP_STATE);
    }
}
