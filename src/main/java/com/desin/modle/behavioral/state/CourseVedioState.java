package com.desin.modle.behavioral.state;

public abstract class CourseVedioState {
    protected CourseVedioContext courseVedioContext;

    public void setCourseVedioContext(CourseVedioContext courseVedioContext) {
        this.courseVedioContext = courseVedioContext;
    }

    public abstract void play();

    public abstract void speed();

    public abstract void pause();

    public abstract void stop();
}
