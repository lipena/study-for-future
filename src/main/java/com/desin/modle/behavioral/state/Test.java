package com.desin.modle.behavioral.state;

/**
 * 状态模式
 *
 * 适用:一个对象存在多种状态(不同状态下行为不同),且状态可相互转换
 *
 * 优点:将不同状态隔离
 * 把各种状态的转换逻辑,分布到state的子类中，减少相互依赖
 * 增加新的状态非常简单
 *
 * 使用地方:状态机
 */
public class Test {
    public static void main(String[] args) {
        CourseVedioContext courseVedioContext = new CourseVedioContext();
        courseVedioContext.setCourseVedioState(new PlayState());

        System.out.println("当前状态" + courseVedioContext.getCourseVedioState().getClass().getSimpleName());
        courseVedioContext.pause();

        System.out.println("当前状态" + courseVedioContext.getCourseVedioState().getClass().getSimpleName());
        courseVedioContext.speed();

        System.out.println("当前状态" + courseVedioContext.getCourseVedioState().getClass().getSimpleName());
        courseVedioContext.stop();

        System.out.println("当前状态" + courseVedioContext.getCourseVedioState().getClass().getSimpleName());
        courseVedioContext.speed();
    }
}
