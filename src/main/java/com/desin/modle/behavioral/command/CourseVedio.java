package com.desin.modle.behavioral.command;

/**
 * 执行方法
 */
public class CourseVedio {
    private String courseName;

    public CourseVedio(String courseName) {
        this.courseName = courseName;
    }

    public void openCourseVedio(){
        System.out.println(courseName + "开放");
    }

    public void closeCourseVedio(){
        System.out.println(courseName + "关闭");
    }
}
