package com.desin.modle.behavioral.command;

import java.util.ArrayList;
import java.util.List;

/**
 * 执行命令的对象
 */
public class Staff {
    private final List<Command> commands = new ArrayList<Command>();

    public void addCommand(Command command){
        commands.add(command);
    }

    public void executeCommands(){
        for (Command command : commands) {
            command.execute();
        }
        commands.clear();
    }
}
