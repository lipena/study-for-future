package com.desin.modle.behavioral.command;

/**
 * 命令模式
 *
 * 具体命令实现抽象命令,具体命令聚合执行方法，具体命令执行的执行该命令的方法
 *
 * 执行人聚合抽象命令,执行人可以对抽象命令进行添加和执行
 *
 * 适合：请求调用者和请求的接受者需要解耦，使得调用者和接收者不直接交互
 * 需要抽象出等待执行的行为
 *
 * 优点:降低耦合
 * 容易扩展新命令或一组命令
 *
 * Runnable抽象命令
 */
public class Test {
    public static void main(String[] args) {
        CourseVedio courseVedio = new CourseVedio("java课程视频");
        OpenCourseVedioCommand openCourseVedioCommand = new OpenCourseVedioCommand(courseVedio);
        CloseCourseVedioCommand closeCourseVedioCommand = new CloseCourseVedioCommand(courseVedio);

        Staff staff = new Staff();
        staff.addCommand(openCourseVedioCommand);
        staff.addCommand(closeCourseVedioCommand);

        staff.executeCommands();
    }
}
