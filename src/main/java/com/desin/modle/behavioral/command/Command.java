package com.desin.modle.behavioral.command;

/**
 * 抽象命令
 */
public interface Command {

    void execute();
}
