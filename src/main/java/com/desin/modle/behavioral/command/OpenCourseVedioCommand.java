package com.desin.modle.behavioral.command;

/**
 * 具体命令
 *
 * 聚合执行方法
 *
 * 实现抽象命令
 */
public class OpenCourseVedioCommand implements Command {
    private CourseVedio courseVedio;

    public OpenCourseVedioCommand(CourseVedio courseVedio) {
        this.courseVedio = courseVedio;
    }

    @Override
    public void execute() {
        courseVedio.openCourseVedio();
    }
}
