package com.desin.modle.behavioral.templatemethod;

public class FeCourse extends ACourse {

    @Override
    void packageCourse() {
        System.out.println("包装前端课程");
    }

    @Override
    Boolean needWriteArticleHook() {
        return true;
    }
}
