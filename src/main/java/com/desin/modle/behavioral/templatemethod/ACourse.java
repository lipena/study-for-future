package com.desin.modle.behavioral.templatemethod;

public abstract class ACourse {
    protected final void makeCourse(){
        makePPT();
        makeVedio();
        if(!needWriteArticleHook()){
            writeArticle();
        }
        packageCourse();

    }

    final void makePPT(){
        System.out.println("制作PPT");
    }

    final void makeVedio(){
        System.out.println("制作Vedio");
    }

    final void writeArticle(){
        System.out.println("写手记");
    }

    //写手记的钩子方法
    Boolean needWriteArticleHook(){
        return false;
    }

     abstract void packageCourse();
}
