package com.desin.modle.behavioral.templatemethod;

//模板模式

//AbstractList,HttpServlet
public class Test {
    public static void main(String[] args) {
        ACourse feCourse = new FeCourse();
        feCourse.makeCourse();

        ACourse javaCourse = new JavaCourse();
        javaCourse.makeCourse();
    }
}
