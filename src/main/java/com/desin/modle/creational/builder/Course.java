package com.desin.modle.creational.builder;

public class Course {

    private String courseName;

    private String courseVedio;

    private String courseArticle;

    public Course(CourseBuilder courseBuilder){
        this.courseName = courseBuilder.courseName;
        this.courseVedio = courseBuilder.courseVedio;
        this.courseArticle = courseBuilder.courseArticle;
    }


    public static class CourseBuilder{
        private String courseName;

        private String courseVedio;

        private String courseArticle;

        public CourseBuilder buildCourseName(String courseName){
            this.courseName = courseName;
            return this;
        }

        public CourseBuilder buildCourseVedio(String courseVedio){
            this.courseVedio = courseVedio;
            return this;
        }

        public CourseBuilder buildCourseArticle(String courseArticle){
            this.courseArticle = courseArticle;
            return this;
        }

        public Course build(){
            return new Course(this);
        }
    }

    @Override
    public String toString() {
        return "Course{" +
                "courseName='" + courseName + '\'' +
                ", courseVedio='" + courseVedio + '\'' +
                ", courseArticle='" + courseArticle + '\'' +
                '}';
    }
}
