package com.desin.modle.creational.builder;

/**
 * 建造者模式(频繁)
 * 使用了匿名内部类来充当建造者,来建造具体的对象,注重的对象属性的顺序
 *
 * StringBuilder和StringBuffer就是用的建造者，只是后者加了同步关键字
 *
 * guava里面的CacheBuilder就是个建造者
 *
 * BeanDefinitionBuilder也是个建造者
 */
public class Test {
    public static void main(String[] args) {
        Course course = new Course.CourseBuilder()
                .buildCourseArticle("课程Article")
                .buildCourseName("课程Name")
//                .buildCourseVedio("课程Vedio")
                .build();
        System.out.println(course);
    }
}
