package com.desin.modle.creational.factorymethod;

public class FeVedioFactory extends VedioFactory {
    @Override
    public Vedio product() {
        return new FeVedio();
    }
}
