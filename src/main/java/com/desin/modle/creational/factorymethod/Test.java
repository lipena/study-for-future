package com.desin.modle.creational.factorymethod;


/**
 * 1.工厂方法模式
 * 抽象工厂创建抽象产品,
 * 具体工厂创建具体产品
 * 具体工厂继承抽象工厂,具体产品继承抽象产品
 *
 * JavaVedioFactory和FeVedioFactory是同一产品等级
 *
 * 美的空调和美的电冰箱是同一产品簇
 *
 * Collections.iterator()就是工厂方法,ArrayList.iterator()是实例工厂
 *
 * URLStreamHandlerFactory是抽象工厂,URLStreamHandler是抽象产品,Factory是实例工厂
 *
 * 一个抽象工厂的具体工厂是创建一个抽象产品族的具体产品族
 */
public class Test {
    public static void main(String[] args) {
        VedioFactory vedioFactory1 = new JavaVedioFactory();
        VedioFactory vedioFactory2 = new FeVedioFactory();
        Vedio product = vedioFactory1.product();
        Vedio product1 = vedioFactory2.product();
        product.broadcast();
        product1.broadcast();
    }
}
