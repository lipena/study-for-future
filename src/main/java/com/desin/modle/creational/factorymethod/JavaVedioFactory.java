package com.desin.modle.creational.factorymethod;

public class JavaVedioFactory extends VedioFactory {
    @Override
    public Vedio product() {
        return new JavaVedio();
    }
}
