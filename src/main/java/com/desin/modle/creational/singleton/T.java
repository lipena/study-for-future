package com.desin.modle.creational.singleton;

public class T implements Runnable{

    public void run() {
        LazySingleton lazySingleton = LazySingleton.getInstance();
        System.out.println("Thread name is " + Thread.currentThread().getName() + " lazySingleton is" + lazySingleton);
    }
}
