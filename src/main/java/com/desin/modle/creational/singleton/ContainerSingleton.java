package com.desin.modle.creational.singleton;

import java.util.HashMap;
import java.util.Map;

//容器单例
//能够对所有单例做容器化的管理
//hashMap最好不要替换为HashTable,否则极为影响性能
//无法保证多线程安全
//序列化和反射攻击都不好
//看具体使用业务逻辑决定
public class ContainerSingleton {

    private static Map<String ,Object> objectMap = new HashMap<String, Object>();

    private ContainerSingleton(){

    }

    public static void putInstance(String key,Object instance){
        if(!objectMap.containsKey(key) && instance!=null){
            objectMap.put(key,instance);
        }
    }

    public static Object getInstance(String key){
        return objectMap.get(key);
    }
}
