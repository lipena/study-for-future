package com.desin.modle.creational.singleton;

import java.io.Serializable;

//饿汉式单例,在类加载的时候就把对象创建,可以避免多线程问题
//但是如果对象不被用到,则会造成一定的资源浪费
//能防止反射攻击
//RunTime类是用的这种模式
public class HungrySingleton implements Serializable {

    private final static HungrySingleton SINGLETON = new HungrySingleton();

    private HungrySingleton(){
        if(SINGLETON!=null){
            throw new RuntimeException("单例模式不允许反射攻击");
        }
    }

    public static HungrySingleton getInstance(){
        return SINGLETON;
    }

    //防止序列化攻击
    public Object readResolve(){
        return SINGLETON;
    }
}
