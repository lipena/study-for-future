package com.desin.modle.creational.singleton;

public class EnumLazySinleton {
    private static EnumLazySinleton instance = null;

    // 私有构造函数
    private EnumLazySinleton(){
    }

    public static EnumLazySinleton getInstance(){
        return Sinleton.SINLETON.getInstance();
    }

    private enum Sinleton{
        SINLETON;

        private EnumLazySinleton singleton;

        // JVM保证这个方法只调用一次
        Sinleton(){
            singleton = new EnumLazySinleton();
        }

        public EnumLazySinleton getInstance(){
            return singleton;
        }
    }
}
