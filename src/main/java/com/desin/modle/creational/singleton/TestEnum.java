package com.desin.modle.creational.singleton;

import java.io.*;
import java.lang.reflect.InvocationTargetException;

public class TestEnum {
    public static void main(String[] args) throws IOException, ClassNotFoundException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
//        EnumInstanceSingleton enumInstanceSingleton = EnumInstanceSingleton.getInstance();
//        enumInstanceSingleton.setDate(new Object());
//
//        ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream("singleton_file"));
//        objectOutputStream.writeObject(enumInstanceSingleton);
//
//        File file = new File("singleton_file");
//        ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(file));
//
//        EnumInstanceSingleton newenumInstanceSingleton = (EnumInstanceSingleton) objectInputStream.readObject();
//        System.out.println(enumInstanceSingleton.date);
//        System.out.println(newenumInstanceSingleton.date);
//        System.out.println(enumInstanceSingleton.date==newenumInstanceSingleton.date);

        EnumInstanceSingleton enumInstanceSingleton = EnumInstanceSingleton.getInstance();
        enumInstanceSingleton.setDate(new Object());
        enumInstanceSingleton.print();

//        Constructor<EnumInstanceSingleton> declaredConstructor = EnumInstanceSingleton.class.getDeclaredConstructor(String.class,int.class);
//        declaredConstructor.setAccessible(true);
//
//        EnumInstanceSingleton newenumInstanceSingleton = declaredConstructor.newInstance();
//
//        System.out.println(enumInstanceSingleton.date);
//        System.out.println(newenumInstanceSingleton.date);
//        System.out.println(enumInstanceSingleton.date==newenumInstanceSingleton.date);
    }
}
