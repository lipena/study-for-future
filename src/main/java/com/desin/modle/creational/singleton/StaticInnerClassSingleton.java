package com.desin.modle.creational.singleton;

//静态内部类式单例---基于类初始化的延迟加载解决方案(用的是静态内部类的Class对象初始化锁)
//保证对象初始化的时候重排序问题不可见
//可以避免多线程问题(通过静态内部类的Class对象初始化锁来避免多线程问题)
//能防止反射攻击
//有点类似饿汉式
public class StaticInnerClassSingleton {
    private static class InnerClass{
        private static StaticInnerClassSingleton staticInnerClassSingleton = new StaticInnerClassSingleton();
    }

    private StaticInnerClassSingleton(){
       if(InnerClass.staticInnerClassSingleton!=null){
           throw new RuntimeException("单例模式不允许反射攻击");
       }
    }

    public static StaticInnerClassSingleton getInstance(){
        return InnerClass.staticInnerClassSingleton;
    }
}
