package com.desin.modle.creational.singleton;

//懒汉式
//不加入synchronized关键字的话，则多线程会有问题
//不能防止反射攻击
public class LazySingleton {
    private static LazySingleton lazySingleton = null;

    private LazySingleton(){

    }

//    public static LazySingleton getInstance(){
//        if(lazySingleton == null){
//            lazySingleton = new LazySingleton();
//        }
//        return lazySingleton;
//    }

    //如果synchronized锁中有static，则是锁的这个LazySingleton类,如果没有static，则相当于锁的是堆内存中的对象
    public synchronized static LazySingleton getInstance(){
        if(lazySingleton == null){
            lazySingleton = new LazySingleton();
        }
        return lazySingleton;
    }
}
