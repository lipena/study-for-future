package com.desin.modle.creational.singleton;

//枚举单例
//多线程安全
//防止反射攻击
//防止序列化攻击
public enum EnumInstanceSingleton {
    ENUM_INSTANCE_SINGLETON{
        protected void print(){
            System.out.println("Hello world!");
        }
    };

    protected abstract void print();

    public Object date;

    public Object getDate() {
        return date;
    }

    public void setDate(Object date) {
        this.date = date;
    }

    public static EnumInstanceSingleton getInstance(){
        return ENUM_INSTANCE_SINGLETON;
    }
}
