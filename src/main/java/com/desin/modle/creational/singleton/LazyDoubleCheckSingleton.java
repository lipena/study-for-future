package com.desin.modle.creational.singleton;

//懒汉式双层检查
//不能防止反射攻击
public class LazyDoubleCheckSingleton {
    private volatile static LazyDoubleCheckSingleton lazyDoubleCheckSingleton = null;

    private LazyDoubleCheckSingleton(){

    }

    public static LazyDoubleCheckSingleton getInstance(){
        if(lazyDoubleCheckSingleton==null){
            synchronized (LazyDoubleCheckSingleton.class){
                if(lazyDoubleCheckSingleton==null){
                    lazyDoubleCheckSingleton = new LazyDoubleCheckSingleton();
                    //1.分配内存给这个对象
                    //2.初始化对象
                    //3.设置lazyDoubleCheckSingleton指向刚分配的内存

                    //由于单线程内创建对象会出现重排序的问题，及第2步和第3步会颠倒,加入volatile关键字会消除重排序


                    //多线程对同一对象写操作一定要加入锁
                }
            }
        }
        return lazyDoubleCheckSingleton;
    }
}
