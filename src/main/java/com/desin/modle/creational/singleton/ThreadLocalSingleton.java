package com.desin.modle.creational.singleton;

//ThreadLocal单例模式
//能保证单个线程内的单例
//mybaties 里面的ErrorContext就是用的这种模式
public class ThreadLocalSingleton {
    private static ThreadLocal<ThreadLocalSingleton> threadLocalSingletonThreadLocal
            = new ThreadLocal<ThreadLocalSingleton>(){
        @Override
        protected ThreadLocalSingleton initialValue() {
            return new ThreadLocalSingleton();
        }
    };

    private ThreadLocalSingleton(){

    }

    public static ThreadLocalSingleton getInstance(){
        return threadLocalSingletonThreadLocal.get();
    }
}
