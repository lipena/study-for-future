package com.desin.modle.creational.abstractfactory;

public class FeCourseFactory implements CourseFactory {
    public Vedio makeVedio() {
        return new FeVedio();
    }

    public Articl makeArticl() {
        return new FeArticl();
    }
}
