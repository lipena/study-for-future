package com.desin.modle.creational.abstractfactory;

public class JavaCourseFactory implements CourseFactory {
    public Vedio makeVedio() {
        return new JavaVedio();
    }

    public Articl makeArticl() {
        return new JavaArticl();
    }
}
