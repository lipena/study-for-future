package com.desin.modle.creational.abstractfactory;

/**
 * 抽象工厂模式
 *
 * 一个抽象工厂生产抽象产品族
 * 一个抽象工厂中的某个方法生产对应的某个抽象产品
 * 一个具体工厂生产一个具体的产品族
 * 一个具体工厂的某个方法生产对应的某个具体产品
 * 具体工厂实现抽象工厂
 * 具体产品继承抽象产品
 *
 * java.sql.Connection就是一个抽象工厂
 */
public class Test {
    public static void main(String[] args) {
        CourseFactory courseFactory = new JavaCourseFactory();
        Vedio vedio = courseFactory.makeVedio();
        Articl articl = courseFactory.makeArticl();
        vedio.produce();
        articl.produce();
    }
}
