package com.desin.modle.structural.composite;

/**
 * 组合模式
 *
 * 目标是将整体和局部当做一体来处理
 *
 * 主要是处理树装结构
 *
 * 抽象组件,对象继承抽象组件并利用抽象组件来操作
 *
 * java.awt.container
 * HashMap,ArrayList等
 * MixedSqlNode是根节点(类似CourseCatelog),sqlNode是抽象组件
 */
public class Test {
    public static void main(String[] args) {
        CatelogComponent linuxCourse = new Course("linux课程",100);
        CatelogComponent windowsCourse = new Course("windows课程",50);

        CatelogComponent javaCourseCatelog = new CourseCatelog("java课程目录",2);
        CatelogComponent javaCourse1 = new Course("java课程1",30);
        CatelogComponent javaCourse2 = new Course("java课程2",40);
        CatelogComponent javaCourse3 = new Course("java课程3",50);
        javaCourseCatelog.add(javaCourse1);//这个地方如果有static关键字,那么就会往唯一一个内存放入javaCourse1
        javaCourseCatelog.add(javaCourse2);
        javaCourseCatelog.add(javaCourse3);

        CatelogComponent rootCourseCatelog = new CourseCatelog("根目录",1);
        rootCourseCatelog.add(linuxCourse);
        rootCourseCatelog.add(windowsCourse);
        //这个地方如果有static关键字,那么就会往唯一一个内存放入javaCourseCatelog,造成循序依赖
//        rootCourseCatelog.add(javaCourseCatelog);

        rootCourseCatelog.print();
    }
}
