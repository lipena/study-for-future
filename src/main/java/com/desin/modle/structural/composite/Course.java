package com.desin.modle.structural.composite;

public class Course extends CatelogComponent {
    private String name;

    private double price;

    public Course(String name, double price) {
        this.name = name;
        this.price = price;
    }

    @Override
    public String getName(CatelogComponent catelogComponent) {
        return name;
    }

    @Override
    public double getPrice(CatelogComponent catelogComponent) {
        return price;
    }

    @Override
    public void print() {
        System.out.println(name + " 价格:" +price);
    }
}
