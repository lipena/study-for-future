package com.desin.modle.structural.composite;

public abstract class CatelogComponent {
    public void add(CatelogComponent catelogComponent){
        throw new RuntimeException("不支持add操作");
    }

    public void remove(CatelogComponent catelogComponent){
        throw new RuntimeException("不支持remove操作");
    }

    public String getName(CatelogComponent catelogComponent){
        throw new RuntimeException("不支持getName操作");
    }

    public double getPrice(CatelogComponent catelogComponent){
        throw new RuntimeException("不支持getPrice操作");
    }

    public void print(){
        throw new RuntimeException("不支持print操作");
    }
}
