package com.desin.modle.structural.composite;

import org.omg.PortableInterceptor.INACTIVE;

import java.util.ArrayList;
import java.util.List;

public class CourseCatelog extends CatelogComponent{
    private String name;

    private Integer level;

    //TODO 这个地方加入static关键字之后会有循环依赖的问题，有待考证
    //这个地方加入static关键字之后,就在内存中唯一一个地方
    //加入statio关键字之后,这个属性就是跟着类了，而不是跟着对象
    private List<CatelogComponent> CATELOG_COMPONENTS = new ArrayList<CatelogComponent>();

    public CourseCatelog(String name, Integer level) {
        this.name = name;
        this.level = level;
    }

    @Override
    public void add(CatelogComponent catelogComponent) {
        CATELOG_COMPONENTS.add(catelogComponent);
    }

    @Override
    public void remove(CatelogComponent catelogComponent) {
        CATELOG_COMPONENTS.remove(catelogComponent);
    }

    @Override
    public String getName(CatelogComponent catelogComponent) {
        return name;
    }

    @Override
    public void print() {
        System.out.println(name);
        for (CatelogComponent catelogComponent : CATELOG_COMPONENTS) {
            if (this.level != null){
                for(int i=0;i<level;i++){
                    System.out.print("  ");
                }
            }
            catelogComponent.print();
        }
    }
}
