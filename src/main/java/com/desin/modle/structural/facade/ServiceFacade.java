package com.desin.modle.structural.facade;

/**
 * 外观模式,可以用来做版本控制,整一个抽象外观类，然后每个版本对应一个实体外观类,符合迪米特原则，但是破坏了开闭原则
 *
 * jdbcUtils就是外观模式,BeanUtils是使用的浅克隆
 *
 * Tomcat里面有大量使用Facade的外观模式,比如RequestFacade,ResponseFacade
 */
public class ServiceFacade {
    private ServiceOne serviceOne = new ServiceOne();

    private ServiceTwo serviceTwo = new ServiceTwo();

    private ServiceThree serviceThree = new ServiceThree();

    public  void ServiceFacadeTest(PointGift pointGift){
        boolean b = serviceOne.testOne(pointGift);
        boolean b1 = serviceTwo.testTwo(pointGift);
        String s = serviceThree.testThree(pointGift);
    }
}
