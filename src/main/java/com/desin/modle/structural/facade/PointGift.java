package com.desin.modle.structural.facade;

public class PointGift {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PointGift(){

    }

    public PointGift(String name){
        this.name = name;
    }
}
