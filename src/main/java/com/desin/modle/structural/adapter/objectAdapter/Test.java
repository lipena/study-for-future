package com.desin.modle.structural.adapter.objectAdapter;

//对象适配器
//用的是组合关系
//优先

//xmlAdapter目标接口,JpaVendorAdapter目标接口,HandleAdapter目标接口
public class Test {
    public static void main(String[] args) {
        Target target = new CourentTarget();
        target.request();

        Target adapter = new Adapter();
        adapter.request();
    }
}
