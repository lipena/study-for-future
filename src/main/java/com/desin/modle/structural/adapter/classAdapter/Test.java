package com.desin.modle.structural.adapter.classAdapter;

//类适配器
//用的是继承关系
public class Test {
    public static void main(String[] args) {
        Target target = new CourentTarget();
        target.request();

        Target adapter = new Adapter();
        adapter.request();
    }
}
