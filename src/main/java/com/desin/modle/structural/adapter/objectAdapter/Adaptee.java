package com.desin.modle.structural.adapter.objectAdapter;

/**
 * 被适配者
 */
public class Adaptee {
    public void adapteeRequest(){
        System.out.println("被适配器请求方法");
    }
}
