package com.desin.modle.structural.adapter.objectAdapter;

/**
 * 目标接口
 */
public interface Target {
    void request();
}
