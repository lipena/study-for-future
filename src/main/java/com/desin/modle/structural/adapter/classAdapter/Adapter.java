package com.desin.modle.structural.adapter.classAdapter;

/**
 * 适配器
 */
public class Adapter extends Adaptee implements Target{
    @Override
    public void request() {
       super.adapteeRequest();
    }
}
