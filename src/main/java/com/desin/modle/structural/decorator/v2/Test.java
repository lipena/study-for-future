package com.desin.modle.structural.decorator.v2;

//装饰者模式
//BufferedReader抽象裝飾者，BufferedInputStream具体装饰者,TransactionAwareCacheDecorator

//ServletRequestWrapper抽象装饰者.HttpServletRequestWrapper实体装饰者
//ServletRequest抽象实体

//Cache抽象实体,LruCache抽象装饰者
//有时候抽象装饰者可具体装饰者又没那么多的限制，可以不要具体装饰者

//抽象装饰者继承抽象实体,并且里面还有抽象实体的引用，并且抽象装饰者的构造方法含有抽象实体引用对象初始化
public class Test {
    public static void main(String[] args) {
        AbstractJianBing abstractJianBing = new JianBing();

        abstractJianBing = new EggDecorator(abstractJianBing);
        abstractJianBing = new XCDecorator(abstractJianBing);
        abstractJianBing = new XCDecorator(abstractJianBing);
        System.out.println(abstractJianBing.desc() + " 销售价格:" + abstractJianBing.cost());
    }
}
