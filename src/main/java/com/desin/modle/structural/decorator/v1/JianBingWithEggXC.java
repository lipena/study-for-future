package com.desin.modle.structural.decorator.v1;

public class JianBingWithEggXC extends JianBingWithEgg {
    @Override
    public String desc() {
        return super.desc()+"加一个香肠";
    }

    @Override
    public Integer cost() {
        return super.cost()+2;
    }
}
