package com.desin.modle.structural.decorator.v1;

public class JianBingWithEgg extends JianBing {
    @Override
    public String desc() {
        return super.desc()+"加一个鸡蛋";
    }

    @Override
    public Integer cost() {
        return super.cost()+1;
    }
}
