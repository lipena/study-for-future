package com.desin.modle.structural.decorator.v2;

//具体实体
public class JianBing extends AbstractJianBing {
    @Override
    public String desc() {
        return "煎饼";
    }

    @Override
    public Integer cost() {
        return 8;
    }
}
