package com.desin.modle.structural.decorator.v2;

//具体装饰者
public class EggDecorator extends AbstractDecorator {
    public EggDecorator(AbstractJianBing abstractJianBing) {
        super(abstractJianBing);
    }

    @Override
    public void dosomething() {
        //做部分事情
    }

    @Override
    public String desc() {
        return super.desc() + "加一个鸡蛋";
    }

    @Override
    public Integer cost() {
        return super.cost() + 1;
    }
}
