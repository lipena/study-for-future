package com.desin.modle.structural.decorator.v2;

//抽象装饰者 拥有抽象实体的引用，可以将抽象实体的属性一直往下传递
public abstract class AbstractDecorator extends AbstractJianBing {
    private AbstractJianBing abstractJianBing;

    public abstract void dosomething();

    public AbstractDecorator(AbstractJianBing abstractJianBing){
        this.abstractJianBing = abstractJianBing;
    }

    @Override
    public String desc() {
        return abstractJianBing.desc();
    }

    @Override
    public Integer cost() {
        return abstractJianBing.cost();
    }

}
