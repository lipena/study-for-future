package com.desin.modle.structural.decorator.v2;

//具体装饰者
public class XCDecorator extends AbstractDecorator{
    public XCDecorator(AbstractJianBing abstractJianBing) {
        super(abstractJianBing);
    }

    @Override
    public void dosomething() {
        //做部分事情
    }

    @Override
    public String desc() {
        return super.desc() + "加一根香肠";
    }

    @Override
    public Integer cost() {
        return super.cost() + 2;
    }
}
