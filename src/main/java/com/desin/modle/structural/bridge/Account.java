package com.desin.modle.structural.bridge;

public interface Account {
    Account openAccount();

    void showAccountType();
}
