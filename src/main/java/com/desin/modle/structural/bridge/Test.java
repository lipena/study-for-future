package com.desin.modle.structural.bridge;

/**
 * 桥接模式
 *
 * 将抽象部分与具体实现部分完全分离,使两边能够同时扩展
 *
 * 适合多钟变化的条件，比如这块银行和账户类型可以组成不同的银行账户
 *
 * 其中Account和Bank是两个完全平级的，可以互相颠倒(Account用抽象类,Bank用接口)
 *
 * 注意要将方法委托给其他对象去实施
 *
 * Connection是接口(类似Account),mysql和orcale的Connection是具体实现,DriverManager(类似Bank)
 */
public class Test {
    public static void main(String[] args) {
        Bank hbBank = new HbBank(new DepositAccount());
        Account account = hbBank.openAccount();
        account.showAccountType();

        Bank icbcBank = new IcbcBank(new SavingAccout());
        Account account1 = icbcBank.openAccount();
        account1.showAccountType();

        Bank huBank1 = new HbBank(new SavingAccout());
        Account account2 = huBank1.openAccount();
        account2.showAccountType();
    }
}
