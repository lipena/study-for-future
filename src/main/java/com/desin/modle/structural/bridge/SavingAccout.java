package com.desin.modle.structural.bridge;

public class SavingAccout implements Account{
    public Account openAccount() {
        System.out.println("打开活期账户");
        return new SavingAccout();
    }

    public void showAccountType() {
        System.out.println("这是一个活期账户类型");
    }
}
