package com.desin.modle.structural.bridge;

public class DepositAccount implements Account{
    public Account openAccount() {
        System.out.println("打开定期账户");
        return new DepositAccount();
    }

    public void showAccountType() {
        System.out.println("这是定期账户类型");
    }
}
