package com.desin.modle.structural.bridge;

public class IcbcBank extends Bank {
    public IcbcBank(Account account) {
        super(account);
    }

    public Account openAccount() {
        System.out.println("打开工商银行账户");
        account.openAccount();//调用委托的方法
        return account;
    }
}
