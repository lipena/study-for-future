package com.desin.modle.structural.bridge;

public class HbBank extends Bank {
    public HbBank(Account account) {
        super(account);
    }

    public Account openAccount() {
        System.out.println("打开湖北银行账户");
        account.openAccount();//调用委托的方法
        return this.account;
    }
}
