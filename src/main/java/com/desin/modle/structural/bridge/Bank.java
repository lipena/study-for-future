package com.desin.modle.structural.bridge;

public abstract class Bank {
    Account account;

    public Bank(Account account){
        this.account = account;
    }

    public abstract Account openAccount();
}
