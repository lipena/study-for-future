package com.desin.modle.structural.flyweight;

//Integer.valueOf用到了享元模式

//连接池都是用的享元模式的思维
public class Test {
    private static final String departments[] = {"RD","QA","PM","BD"};

    public static void main(String[] args) {
        for(int i= 0;i<10;i++){
            String department = departments[(int)(Math.random() * departments.length)];
            Manager manager = (Manager) EmployeeFactory.getEmployee(department);
            manager.report();
        }
    }
}
