package com.desin.modle.structural.flyweight;

import java.util.HashMap;
import java.util.Map;

//享元模式
//享元工厂类
public class EmployeeFactory {
    private final static Map<String,Employee> EMPLOYEE_MAP = new HashMap<String, Employee>();

    public static Employee getEmployee(String key){
        Manager manager = (Manager) EMPLOYEE_MAP.get(key);

        if (manager == null){
            manager = new Manager(key);
            System.out.print("创建 " + manager.getLevel() + ":" + key);
            String reportContent = key + "部门汇报:此次汇报的主题";
            manager.setContent(reportContent);
            System.out.println(" 创建报告：" + reportContent);
            EMPLOYEE_MAP.put(key,manager);

        }
        return manager;
    }
}
