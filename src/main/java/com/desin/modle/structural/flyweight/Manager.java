package com.desin.modle.structural.flyweight;

//享元模式
//具体享元类
//分为外部状态和内部状态
public class Manager implements Employee{
    private String department;//外部状态

    private String content;

    private String level = "部门经理";//内部状态

    public void report() {
        System.out.println(content);
    }

    public String getLevel() {
        return level;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Manager(String department){
        this.department = department;
    }
}
