package com.desin.modle.datastruct.queue;

public class MyArrayCycleQueue {
    private int[] data;
    private int cap;
    private int head;
    private int tail;

    public MyArrayCycleQueue(int cap){
        data = new int[cap];
        head=0;
        tail=0;
        this.cap=cap;
    }

    public boolean push(int val){
        if((tail+1)%cap==head){
            return false;
        }
        data[tail] = val;
        tail=(tail+1)%cap;
        return true;
    }

    public int pop(){
        if(tail==head){
            return -1;
        }
        int val = data[head];
        head=(head+1)%cap;
        return val;
    }
}
