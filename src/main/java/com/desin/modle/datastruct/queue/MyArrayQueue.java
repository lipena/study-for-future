package com.desin.modle.datastruct.queue;

public class MyArrayQueue {
    private int[] data;
    private int count;
    private int head;
    private int cap;

    public MyArrayQueue(int cap){
        data = new int[cap];
        count=0;
        head=0;
        this.cap=cap;
    }

    public boolean push(int x){
        if(count>=cap){
            if(count-head<cap){
                if(count==head){
                    count=0;
                    head=0;
                }else{
                    int length = data.length;
                    for(int i=head;i<length;i++){
                        data[i-head]=data[i];
                    }
                    count=count-head;
                    head=0;
                }
            }else{
                return false;
            }
        }
        this.data[count]=x;
        count++;
        return true;
    }

    public int pop(){
        if (count-head == 0) {
            return -1;
        }
        int val = this.data[head];
        head++;
        return val;
    }
}
