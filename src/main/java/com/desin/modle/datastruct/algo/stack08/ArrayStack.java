package com.desin.modle.datastruct.algo.stack08;

import java.util.concurrent.locks.Condition;

public class ArrayStack {
    private int[] data;

    private int count;

    private int capacity;

    public ArrayStack (int capacity){
        this.data = new int[capacity];
        this.count=0;
        this.capacity=capacity;
    }

    public boolean push(int val){
        if(count>=capacity){
            return false;
        }
        data[count] = val;
        count++;
        return true;
    }

    public int pop(){
        if(count==0){
            return -1;
        }
        int val = data[count-1];
        count--;
        return val;
    }

    public int size(){
        return count;
    }
}
