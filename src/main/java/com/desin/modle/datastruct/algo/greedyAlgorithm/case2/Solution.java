package com.desin.modle.datastruct.algo.greedyAlgorithm.case2;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Solution {
    PriorityQueue<Sweet> sweetPriorityQueue = new PriorityQueue<>(
            (o1, o2) -> {
                if(o1.getType()>o2.getType()){
                    return 1;
                }else if(o1.getType()<o2.getType()){
                    return -1;
                }
                return 0;
            }
    );

    PriorityQueue<Child> childPriorityQueue = new PriorityQueue<>(
            (o1, o2) -> {
                if(o1.getType()>o2.getType()){
                    return 1;
                }else if(o1.getType()<o2.getType()){
                    return -1;
                }
                return 0;
            }
    );

    /**
     * 添加糖果信息
     *
     * @param name 名称
     * @param num 数量
     * @param type 类型，
     */
    public void addSweet(String name, int num, int type) {
        this.sweetPriorityQueue.offer(new Sweet(name, num, type));
    }

    /**
     * 添加孩子信息，以及期望的糖果
     *
     * @param name 孩子的名称
     * @param type 类型信息
     */
    public void addChild(String name, int type) {
        this.childPriorityQueue.offer(new Child(name, type));
    }

    public List<Child> get(){
        List<Child> childList = new ArrayList<>();
        while (!sweetPriorityQueue.isEmpty()){
            Sweet sweet = sweetPriorityQueue.poll();

            if(sweet.getNum()>0){
                Child child = childPriorityQueue.poll();
                if(child.getType()<=sweet.getType()){
                    child.setSweet(sweet);
                    childList.add(child);
                    int i = sweet.getNum() - 1;
                    sweet.setNum(sweet.getNum()-1);
                }else {
                    childPriorityQueue.offer(child);
                }
            }
            if(sweet.getNum()>0) {
                sweetPriorityQueue.offer(sweet);
            }

        }
        return childList;
    }
}
