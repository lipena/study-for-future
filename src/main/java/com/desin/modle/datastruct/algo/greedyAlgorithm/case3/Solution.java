package com.desin.modle.datastruct.algo.greedyAlgorithm.case3;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Solution {
    PriorityQueue<ScopeBusi> priorityQueue = new PriorityQueue<>(
            (o1, o2) -> {
                if(o1.getEnd()>o2.getEnd()){
                    return 1;
                }else if(o1.getEnd()<o2.getEnd()){
                    return -1;
                }
                return 0;
            }
    );

    public List<ScopeBusi> get(){
        List<ScopeBusi> scopeBusiList = new ArrayList<>();
        ScopeBusi pollScopeBusi = priorityQueue.poll();
        scopeBusiList.add(pollScopeBusi);

        while (!priorityQueue.isEmpty()){
            ScopeBusi scopeBusi = priorityQueue.poll();
            if(scopeBusi.getStart()>=pollScopeBusi.getEnd()){
                scopeBusiList.add(scopeBusi);
                pollScopeBusi = scopeBusi;
            }
        }
        return scopeBusiList;
    }
}
