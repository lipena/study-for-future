package com.desin.modle.datastruct.algo.stack08;

import com.desin.modle.datastruct.algo.linkedlist06.MyLinkedNode;

import java.util.LinkedList;

public class ListNodeStack {
    private MyLinkedNode data;
    private int conun;

    public ListNodeStack(){
        this.data = new MyLinkedNode();
        conun=0;
    }

    public boolean push(int value){
        data.add(value);
        conun++;
        return true;
    }

    public int pop(){
        int lastValue = data.deleteLast();
        return lastValue;
    }

    public int size(){
        return conun;
    }
}
