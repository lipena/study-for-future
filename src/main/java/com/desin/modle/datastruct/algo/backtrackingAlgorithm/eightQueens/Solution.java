package com.desin.modle.datastruct.algo.backtrackingAlgorithm.eightQueens;

public class Solution {
    private static final int DEFALUT_SIZE = 8;

    private int[] result = new int[DEFALUT_SIZE];

    public void soultion(int row){
        if(row==DEFALUT_SIZE){
            print(result);
            return;
        }
        for(int column=0;column<DEFALUT_SIZE;column++){
            if(isOK(row,column)){
                result[row]=column;
                soultion(row+1);
            }
        }
    }

    private boolean isOK(int row, int column) {
        int leftUp = column-1;
        int rightUp = column+1;
        for(int i=row-1;i>=0;i--){
            if(result[i]==column){
                return false;
            }
            if(leftUp>=0&&result[i]==leftUp){
                return false;
            }
            if(rightUp<=DEFALUT_SIZE&&result[i]==rightUp){
                return false;
            }
            leftUp--;
            rightUp++;
        }
        return true;
    }


    private void print(int[] result){
        for(int row=0;row<DEFALUT_SIZE;row++){
            for(int column=0;column<DEFALUT_SIZE;column++){
                if(result[row]==column){
                    System.out.print("Q ");
                }else{
                    System.out.print("* ");
                }
            }
            System.out.println();
        }
        System.out.println();
    }
}
