package com.desin.modle.datastruct.algo.greedyAlgorithm.case1;

import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;

public class Solution {

    PriorityQueue<Goods> priorityQueue = new PriorityQueue<Goods>(
            ((o1, o2) -> {
                if(o1.getUnitPrice()<o2.getUnitPrice()){
                    return 1;
                }else if(o1.getUnitPrice()>o2.getUnitPrice()){
                    return -1;
                }
                return 0;
            })
    );

    public void addGoods(Goods goods){
        this.priorityQueue.offer(goods);
    }

    public List<Goods> getMaxValueGoods(int weight){
        int sourceWeight = weight;
        ArrayList<Goods> goodsList = new ArrayList<>();

        while (!priorityQueue.isEmpty()){
            Goods goods = priorityQueue.poll();
            if(sourceWeight>=goods.getWeight()){
                sourceWeight = sourceWeight-goods.getWeight();
                goodsList.add(goods);
            }else {
                if(sourceWeight == 0){
                    break;
                }
                int targetWeight = goods.getWeight() - sourceWeight;
                Goods goods1 = new Goods(goods.getName(), sourceWeight, sourceWeight * goods.getUnitPrice(), goods.getUnitPrice());
                goodsList.add(goods1);
                goods.setWeight(targetWeight);
                priorityQueue.offer(goods);
                break;
            }
        }
        return goodsList;
    }
}
