package com.desin.modle.datastruct.algo.stack08;

import java.util.Stack;

public class StackBrower {
    private String curPage;
    private Stack<String> backStacks;
    private Stack<String> forwedStacks;

    public StackBrower(){
        backStacks=new Stack<>();
        forwedStacks=new Stack<>();
    }

    public String goBack(){
        if(canBack()){
            String page = backStacks.pop();
            forwedStacks.push(page);
            curPage=page;
            return page;
        }
        return null;
    }

    public String goForwed(){
        if(canForwed()){
            String page = forwedStacks.pop();
            backStacks.push(page);
            curPage=page;
            return page;
        }
        return null;
    }

    public boolean canBack(){
        if(!backStacks.isEmpty()){
            return true;
        }
        return false;
    }

    public boolean canForwed(){
        if(!forwedStacks.isEmpty()){
            return true;
        }
        return false;
    }

    public void open(String url){
        if(curPage!=null){
            backStacks.push(url);
            forwedStacks.clear();
        }
        showUrl(url,"open");
    }

    public void showUrl(String url,String prefix){
        this.curPage=url;
    }
}
