package com.desin.modle.datastruct.algo.linkedlist06;

public class MyLinkedNode {
    class Node{
        private int value;
        private Node next;
        public Node(int value){
            this.value=value;
        }
    }

    private Node root = new Node(-1);//类似哨兵一样
    private Node curNode;
    private int size;
    public MyLinkedNode(){
        curNode=root;
        size=0;
    }

    public void add(int val){
        Node valNode = new Node(val);
        curNode.next = valNode;
        curNode=valNode;
        size++;
    }

    public int deleteLast(){
        Node deleteNode = curNode;
        Node parentNode = parentNode(curNode);
        int deleteValue = deleteNode.value;
        parentNode.next=null;
        size--;
        curNode=parentNode;
        return deleteValue;
    }

    public int size(){
        return size;
    }
    public boolean empty(){
        Node node = root.next;
        if(node==null){
            return true;
        }
        return false;
    }

    public int deleteTop(){
        Node rootNode = root;
        if (rootNode.next==null){
            return -1;
        }
        int deleteValue=rootNode.next.value;
        Node nextNode = rootNode.next.next;
        rootNode.next = nextNode;
        size--;
        return deleteValue;
    }

    public int getLastValue(){
        if(curNode==null){
            return -1;
        }
        return curNode.value;
    }

    public int getValueByIndex(int index){
        Node cur = root.next;
        if(cur==null){
            return -1;
        }
        int valIndex = 0;
        while (valIndex<index){
            cur=cur.next;
            if(cur==null){
                break;
            }
            valIndex++;
        }
        if(cur==null){
            return -1;
        }

        return cur.value;

    }

    public Node parentNode(Node childNode){
        Node temp = root.next;
        while (temp!=null){
            if(temp.next==childNode){
                break;
            }
            temp=temp.next;
        }
        return temp;
    }


}
