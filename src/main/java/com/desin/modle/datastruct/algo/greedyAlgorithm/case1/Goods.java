package com.desin.modle.datastruct.algo.greedyAlgorithm.case1;

public class Goods {
    private String name;

    private Integer weight;

    private float value;

    private float unitPrice;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getWeight() {
        return weight;
    }

    public void setWeight(Integer weight) {
        this.weight = weight;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public float getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(float unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Goods(String name, Integer weight, float value, float unitPrice) {
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.unitPrice = unitPrice;
    }

    public Goods(String name, Integer weight, float value) {
        this.name = name;
        this.weight = weight;
        this.value = value;
        this.unitPrice = value/weight;
    }
}
