package com.desin.modle.datastruct.recursion;

public class MyFiboracci {
    public int solution(int n){
        if(n==1){
            return 1;
        }
        if(n==2){
            return 2;
        }

        return solution(n-1)+solution(n-2);
    }

    public int solutionLoop(int n){
        if(n==1) return 1;
        if(n==2) return 2;

        int a=1,b=1,v=0;
        for(int i=3;i<n;i++){
            v=a+b;
            a=b;
            b=v;
        }
        return v;
    }
}
