package com.desin.modle.datastruct.recursion;

public class MyFactorial {
    public int solution(int n){
        if(n==1){
            return 1;
        }
        return solution(n-1)*n;
    }

    public int solutionLoop(int n){
        if(n==1){
            return 1;
        }

        int v=1;
        for(int i=2;i<=n;i++){
            v=v*i;
        }
        return v;
    }
}
