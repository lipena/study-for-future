package com.desin.modle.datastruct.myleetcode;

import java.util.LinkedList;
import java.util.Queue;

public class StackByQueue {
    Queue<Integer> queue;
    public StackByQueue(){
        queue = new LinkedList<>();
    }

    public void push(Integer num){
        queue.offer(num);
        int size = queue.size();
        if(size>1){
            while (size>0){
                queue.offer(queue.remove());
                size--;
            }
        }
    }

    public Integer pop(){
        return queue.remove();
    }

    public Integer top(){
        return queue.peek();
    }
}
