package com.desin.modle.datastruct.charmatch.bf;

import org.springframework.util.StringUtils;

public class MyBFMatch {
    public int find(String src,String find){
        if(StringUtils.isEmpty(src)||StringUtils.isEmpty(find)){
            return -1;
        }
        char[] srcArr = src.toCharArray();
        char[] findArr = find.toCharArray();
        if(srcArr.length<findArr.length){
            return -1;
        }

        int srcIndex =0;
        int findIndex=-1;

        while(srcIndex<srcArr.length){
            if(srcIndex+findArr.length>srcArr.length){
                break;
            }
            findIndex=-1;
            for(int i=0;i<findArr.length;i++){
                if(srcArr[srcIndex+i]==findArr[i]){
                    if(findIndex==-1) {
                        findIndex = srcIndex + i;
                    }
                }else{
                    findIndex=-1;
                    break;
                }
            }
            if(findIndex!=-1){
                return findIndex;
            }
            srcIndex++;
        }

        return -1;
    }
}
