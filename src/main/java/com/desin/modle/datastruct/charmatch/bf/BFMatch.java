package com.desin.modle.datastruct.charmatch.bf;

public class BFMatch {

    public int find(String src,String find){
        char[] srcChars = src.toCharArray();
        char[] findChars = find.toCharArray();

        int srcIndex = 0;
        int findIndex = -1;

        while (srcIndex < srcChars.length){
            if(srcIndex + findChars.length>srcChars.length){
                break;
            }

            findIndex = -1;
            for(int i=0;i<findChars.length;i++){
                if(srcChars[srcIndex + i] == findChars[i]){
                    if(findIndex == -1){
                        findIndex = srcIndex + i;
                    }
                }else{
                    findIndex = -1;
                    break;
                }
            }
            if(findIndex!=-1){
                return findIndex;
            }
            srcIndex++;
        }
        return -1;
    }
}
