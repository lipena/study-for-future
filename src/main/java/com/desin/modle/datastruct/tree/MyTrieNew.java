package com.desin.modle.datastruct.tree;

public class MyTrieNew {
    class MyTrieNode{
        private char data;
        private MyTrieNode[] child = new MyTrieNode[26];
        private boolean isEnd;
        public MyTrieNode(char c){
            this.data=c;
        }
    }

    private MyTrieNode tree = new MyTrieNode('/');

    public void insert(String str){
        char[] arr = str.toCharArray();
        MyTrieNode temp = tree;
        for(int i=0;i<arr.length;i++){
            int index = arr[i]-'a';
            if(temp.child[index]==null){
                MyTrieNode trieNode = new MyTrieNode(arr[i]);
                temp.child[index]=trieNode;
            }
            temp=temp.child[index];
        }
        temp.isEnd=true;
    }

    public boolean find(String str){
        char[] arr = str.toCharArray();
        MyTrieNode temp = tree;
        for(int i=0;i<arr.length;i++){
            int index = arr[i]-'a';
            if(temp.child[index]==null){
                return false;
            }
            temp=temp.child[index];
        }
        return temp.isEnd;
    }
}
