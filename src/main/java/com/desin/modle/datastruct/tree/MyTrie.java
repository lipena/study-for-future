package com.desin.modle.datastruct.tree;

public class MyTrie {
    private static final char CHARA = 'a';

    public class TrieNode {
        private char data;

        private TrieNode[] children = new TrieNode[26];

        private boolean isEndChar = false;

        public TrieNode(char data) {
            this.data = data;
        }
    }

    private TrieNode root = new TrieNode('/');

    public void insert(String str){
        char[] chars = str.toCharArray();

        TrieNode procNode = root;

        for(int i=0;i<chars.length;i++){
            int ascValue = chars[i] - CHARA;
            if(procNode.children[ascValue]==null){
                TrieNode node = new TrieNode(chars[i]);
                procNode.children[ascValue] = node;
            }
            procNode = procNode.children[ascValue];
        }

        procNode.isEndChar = true;

    }

    public boolean find(String str){
        char[] chars = str.toCharArray();
        TrieNode procNode = root;

        for(int i=0;i<chars.length;i++){
            int ascValue = chars[i] - CHARA;
            if(procNode.children[ascValue].data != chars[i]){
                return false;
            }
            procNode = procNode.children[ascValue];
        }

        return procNode.isEndChar;
    }

}
