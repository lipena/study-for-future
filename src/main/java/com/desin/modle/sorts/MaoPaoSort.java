package com.desin.modle.sorts;

public class MaoPaoSort {
    public int[] sort(int[] arr){
        if(arr==null||arr.length==0){
            return null;
        }
        int length = arr.length;

        for(int i=0;i<length;i++){
            boolean flag = false;
            for(int j=0;j<length-i-1;j++){
                if(arr[j]>arr[j+1]){
                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = arr[j];
                    flag=true;
                }
            }
            if(!flag){
                break;
            }
        }
        return arr;
    }
}
