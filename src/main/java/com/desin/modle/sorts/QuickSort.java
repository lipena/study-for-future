package com.desin.modle.sorts;

public class QuickSort {
    public static void quickSort(int[] a,int n){
        quickSortIntly(a,0,n-1);
    }

    private static void quickSortIntly(int[] a, int p, int r) {
        if(p>=r){
            return;
        }
        int q = partition(a,p,r);
        quickSortIntly(a,p,q-1);
        quickSortIntly(a,q+1,r);

    }

    private static int partition(int[] a, int p, int r) {
        int i = p;
        int pirot = a[r];
        for(int j=p;j<r;j++){
            if(a[j]<pirot){
                if(i==j){
                    i++;
                }else {
                    int temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                    i++;
                }
            }
        }
        int temp = a[i];
        a[i] = a[r];
        a[r] = temp;
        return i;
    }
}
