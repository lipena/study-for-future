package com.desin.modle.sorts;



public class MyQuickSort {
    public void sort(int[] data){
        quickSort(data,0,data.length-1);
    }

    private void quickSort(int[] data, int start, int end) {
        if(start>=end){
            return;
        }
        int q = findIndex(data,start,end);
        quickSort(data,start,q-1);
        quickSort(data,q+1,end);
    }

    private int findIndex(int[] data, int start, int end) {
        int i=start;
        int point = data[end];
        for(int j=start;j<end;j++){
            if(data[j]<point){
                int temp = data[i];
                data[i]=data[j];
                data[j]=temp;
                i++;
            }
        }
        int temp = data[i];
        data[i]=data[end];
        data[end]=temp;
        return i;
    }
}
