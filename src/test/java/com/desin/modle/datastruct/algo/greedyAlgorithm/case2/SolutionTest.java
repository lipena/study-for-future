package com.desin.modle.datastruct.algo.greedyAlgorithm.case2;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class SolutionTest {
    @Test
    public void testSolution() {
        Solution instance = new Solution();
        instance.addSweet("大糖果", 1, 30);
        instance.addSweet("中糖果", 2, 20);
        instance.addSweet("小糖果", 2, 10);

        instance.addChild("小明", 30);
        instance.addChild("小明2", 30);
        instance.addChild("小红", 20);
        instance.addChild("小绿", 20);
        instance.addChild("小紫", 20);
        instance.addChild("小甲", 10);
        instance.addChild("小乙", 10);
        instance.addChild("小丙", 10);

        List<Child> list = instance.get();

        for (Child chil : list) {
            System.out.println(chil);
        }
    }

}