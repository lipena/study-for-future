package com.desin.modle.datastruct.array;

import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class TowSumTest {
    @Test
    public void threeSum() {
        TowSum towSum = new TowSum();
        int aa[] = {-2,0,3,-1,4,0,3,4,1,1,1,-3,-5,4,0};
        List<List<Integer>> lists = towSum.threeSum(aa);

        Set<List<Integer>> lists1 = new HashSet<>(lists);
        System.out.println(lists1);
    }

    @Test
    public void aa(){
        int nums[] = {3,2,3};
        Map<Integer,Integer> map = new HashMap<>();
        List<Integer> list = new ArrayList<>();
        for(int i=0;i<nums.length;i++){
            int cur = nums[i];
            if(map.containsKey(cur)){
                map.put(cur,map.get(cur)+1);
            }else{
                map.put(cur,1);
            }
        }
        for(Map.Entry re:map.entrySet()){
            if((Integer)re.getValue()>(nums.length/3)){
                list.add((Integer)re.getKey());
            }
        }
        System.out.println(list);
    }
}