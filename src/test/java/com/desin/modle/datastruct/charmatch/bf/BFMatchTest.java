package com.desin.modle.datastruct.charmatch.bf;

import org.junit.Test;

import static org.junit.Assert.*;

public class BFMatchTest {

    @Test
    public void find() {
        BFMatch bfMatch = new BFMatch();
        String src = "my name is lipeng";
        String find = "lipeng";

        int findIndex = bfMatch.find(src, find);
        System.out.println("查找的位置索引为:" + findIndex);

        String sub = src.substring(findIndex);
        System.out.println("截取后的字符串为:" + sub);
    }
}