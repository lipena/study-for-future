package com.desin.modle.multithread;

import org.junit.Test;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;

public class CountDownLatchAndCyclicBarrierTest {

    @Test
    public void check() {
        CountDownLatchAndCyclicBarrier countDownLatchAndCyclicBarrier = new CountDownLatchAndCyclicBarrier();
        countDownLatchAndCyclicBarrier.checkAll();
        while(true) {
            try{
                countDownLatchAndCyclicBarrier.check();
            }catch (Exception e){
                e.printStackTrace();
                break;
            }
        }
    }

    @Test
    public void countDown() throws InterruptedException {
        CountDownLatch countDownLatch = new CountDownLatch(3);
        System.out.println("aaaaaa: "+ countDownLatch.getCount());
        countDownLatch.countDown();
        System.out.println("bbbbbb: "+ countDownLatch.getCount());
        countDownLatch.countDown();
        System.out.println("cccccc: "+ countDownLatch.getCount());
        countDownLatch.await();
        System.out.println("dddddd: "+ countDownLatch.getCount());
    }
}