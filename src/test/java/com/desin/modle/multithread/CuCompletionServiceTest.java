package com.desin.modle.multithread;

import org.junit.Test;

import java.util.concurrent.ExecutionException;

import static org.junit.Assert.*;

public class CuCompletionServiceTest {

    @Test
    public void aa() throws ExecutionException, InterruptedException {
        CuCompletionService cuCompletionService = new CuCompletionService();
        cuCompletionService.aa();
    }

    @Test
    public void bb() throws ExecutionException, InterruptedException {
        CuCompletionService cuCompletionService = new CuCompletionService();
        Integer bb = cuCompletionService.bb();
        System.out.println(bb);
    }
}